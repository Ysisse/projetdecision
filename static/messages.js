"use strict";

let id_sujet;

function add_message_HTML_finie(dict){
  console.log(dict);
  const div_message = document.getElementById("messages_contenu");
  const article = document.createElement("article");
  const contenu = document.createElement("p");
  contenu.appendChild(document.createTextNode(dict["contenu"]));
  article.appendChild(contenu);
  const auteur = document.createElement("footer");
  auteur.appendChild(document.createTextNode(dict["auteur"]));
  article.appendChild(auteur);
  div_message.appendChild(article);
}

function add_message_HTML(reponse){
  if(reponse.ok){
    reponse.json().then(add_message_HTML_finie);
  }
}

/**
 * fetch les données des messages depuis l'id du sujet
 * @param {int} id_sujet
 */
function initMessage(id){
    fetch("/message/"+id).then(messageJson);
};

/**
 * transforme la promesse en json
 * @param {promesse} messages_json_datas
 */
function messageJson(messages_json_datas){
    messages_json_datas.json().then(affichageMessage);
};

function initMessages(){
    const div_message = document.getElementById("messages");
    const titre = document.createElement("h3");
    titre.appendChild(document.createTextNode("Messages"));
    div_message.appendChild(titre);
    const contenu = document.createElement("div");
    contenu.id = "messages_contenu";
    div_message.appendChild(contenu);
    // bouton pour ajouter un message
    const bouton_ajout = document.createElement("button");
    bouton_ajout.appendChild(document.createTextNode("Mettre un commentaire"));
    bouton_ajout.onclick = ajouterUnMessage;
    div_message.appendChild(bouton_ajout);


}

function clearMessages(){
    const div_message = document.getElementById("messages_contenu");
    while (div_message.firstChild) {
        div_message.removeChild(div_message.firstChild);
    }
}
/**
 * traite l'affichage des messages
 * @param {json} messages_datas
 */
function affichageMessage(message){
    const div_message = document.getElementById("messages_contenu");
    const article = document.createElement("article");
    const contenu = document.createElement("p");
    contenu.appendChild(document.createTextNode(message.contenu));
    article.appendChild(contenu);
    const auteur = document.createElement("footer");
    auteur.appendChild(document.createTextNode(message.auteur));
    article.appendChild(auteur);
    div_message.appendChild(article);
};

/**
 * fonction pour ajouter un message
 */
function ajouterUnMessage(){
    // on affiche juste ça pour s'assurer que ça fonctionne
    console.log("ajouter un message");
    document.createElement("li");

    const nomInput =  prompt('Nom de l\'auteur');
    if ( nomInput != null && nomInput != "") {
        let descInput =  prompt("Contenu du message");
        if (descInput != null && descInput != "") {
            const dico = {
                'auteur' : nomInput,
                'contenu' : descInput,
                'id_sujet' : id_sujet
            };
            console.log(dico);
            fetch("/message/",
              {
                method: "PUT",
                body: JSON.stringify(dico)
              }).then(add_message_HTML);
        } else {
            console.log("contenu is null");
        }
    } else {
        console.log('auteur is null');
    }
}
initMessages();
