"use strict";

function add_sondage_HTML_finie(dict){
  console.log(dict);
  const divSond = document.getElementById("sondages_contenu");
  const section = document.createElement('section');
  divSond.appendChild(section);
  let titre = document.createElement('h3');
  titre.innerText=dict["question"];
  section.appendChild(titre);
  const reponses = document.createElement('ul');
  reponses.id = "sondages_contenu_reponse_"+dict["id"];
  for(const auteur in dict["reponses"]) {
      const rep=document.createElement("li");
      rep.innerText = auteur + " : " + dict["reponses"][auteur];
      reponses.appendChild(rep);
  }
  section.appendChild(reponses);
  let bouton = document.createElement('button');
  bouton.innerText = "Répondre";
  bouton.onclick = ()=>addReponse(dict["id"]);
  divSond.appendChild(bouton);
}

function add_sondage_HTML(reponse){
  if(reponse.ok){
    reponse.json().then(add_sondage_HTML_finie);
  }
}

function add_reponse_HTML_finie(dict){
  console.log(dict);
  const reponses = document.getElementById("sondages_contenu_reponse_"+dict['id_sondage']);
  const rep=document.createElement("li");
  rep.innerText = dict["auteur"] + " : " + dict["reponse"];
  reponses.appendChild(rep);
}

function add_reponse_HTML(reponse){
  if(reponse.ok){
    reponse.json().then(add_reponse_HTML_finie);
  }
}


/**
 * fetch les données des sondages depuis l'id du sujet
 * @param {int} id_sond
 */
function initSondage(id_sond) {
    fetch("/sondage/" + id_sond).then(sondageJson);
}

/**
 * Initialise la zone d'affichage des sondages
 */
function initSondages() {
    const div_sondage = document.getElementById("sondages");
    const titre = document.createElement("h3");
    titre.appendChild(document.createTextNode("Sondages"));
    div_sondage.appendChild(titre);
    const contenu = document.createElement("div");
    contenu.id = "sondages_contenu";
    div_sondage.appendChild(contenu);

    let bouton = document.createElement('button');
    bouton.innerText = "Créer sondage";
    bouton.onclick = addSondage;
    div_sondage.appendChild(bouton);
}

/**
 * Vide la zone de sondages
 */
function clearSondages() {
    const div_sondage = document.getElementById("sondages_contenu");
    while (div_sondage.firstChild) {
        div_sondage.removeChild(div_sondage.firstChild);
    }
}

/**
 * transforme la promesse en json
 * @param {promesse} sondages_json_datas
 */
function sondageJson(sondages_json_datas) {
    sondages_json_datas.json().then(afficheSondage);
};

/**
 * fonction pour ajouter un sondage
 */
function addSondage(){
    // on affiche juste ça pour s'assurer que ça fonctionne
    console.log("ajouter un sondage");
    const nomInput =  prompt('Question : ');
    if ( nomInput != null && nomInput != "") {
      const dico = {
          'question' : nomInput,
          'id_sujet' : id_sujet
      };
      console.log(dico);
      fetch("/sondage/",
        {
          method: "PUT",
          body: JSON.stringify(dico)
        }).then(add_sondage_HTML);
    } else {
        console.log('question is null');
    }
}

/**
 * fonction pour ajouter une reponse
 */
function addReponse(id_sondage){
    // on affiche juste ça pour s'assurer que ça fonctionne
    console.log("ajouter une reponse");
    const nomInput =  prompt('Entrez votre nom : ');
    if ( nomInput != null && nomInput != "") {
      if (confirm("Votre reponse : Annuler pour false et Ok pour true")) {
          const dico = {
              'auteur' : nomInput,
              'reponse' : true,
              'id_sondage' : id_sondage
          };
          console.log(dico);
          fetch("/reponse/",
            {
              method: "PUT",
              body: JSON.stringify(dico)
            }).then(add_reponse_HTML);
      } else {
          const dico = {
              'auteur' : nomInput,
              'reponse' : false,
              'id_sondage' : id_sondage
          };
          console.log(dico);
          fetch("/reponse/",
            {
              method: "PUT",
              body: JSON.stringify(dico)
            }).then(add_reponse_HTML);
      }
    } else {
        console.log('question is null');
    }
}


function afficheSondage(sondage) {
    const divSond = document.getElementById("sondages_contenu");
    const section = document.createElement('section');
    divSond.appendChild(section);
    let titre = document.createElement('h3');
    titre.innerText=sondage.question;
    section.appendChild(titre);
    const reponses = document.createElement('ul');
    reponses.id = "sondages_contenu_reponse_"+sondage.id;
    for(const auteur in sondage.reponses) {
        const rep=document.createElement("li");
        rep.innerText = auteur + " : " + sondage.reponses[auteur];
        reponses.appendChild(rep);
    }
    section.appendChild(reponses);

    let bouton = document.createElement('button');
    bouton.innerText = "Répondre";
    bouton.onclick = ()=>addReponse(sondage.id);
    divSond.appendChild(bouton);
}

initSondages();
